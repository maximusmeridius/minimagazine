using MiniMagazine.Application.Services;
using MiniMagazine.Application.UnitOfWork;
using Moq;
using System;
using System.Collections.Generic;
using AutoMapper;
using MiniMagazine.Application.Model;
using MiniMagazine.Application.Profiles;
using MiniMagazine.Contracts;
using Xunit;

namespace MiniMagazine.Tests
{
    public class ApplicationUnitTest
    {
        private readonly Mock<IUnitOfWorkFactory> factoryMock;
        private readonly Mock<IUnitOfWork> uowMock;

        public ApplicationUnitTest()
        {
            uowMock = new Mock<IUnitOfWork>();

            uowMock.Setup(x => x.ArticleRepository.Get(It.IsAny<Guid>())).Returns(new Article { Name = "test article" });
            uowMock.Setup(x => x.CommentRepository.GetForArticle(It.IsAny<Guid>())).Returns(new List<Comment> { new Comment { Content = "test comment" } });

            factoryMock = new Mock<IUnitOfWorkFactory>();
            factoryMock.Setup(x => x.Create()).Returns(uowMock.Object);

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new DomainProfile());
            });
        }


        [Fact]
        public void GetArticleTest()
        {
            var service = new SqlArticleService(factoryMock.Object);
            service.Get(Guid.NewGuid());
            uowMock.Verify(x => x.ArticleRepository.Get(It.IsAny<Guid>()), Times.Once);
            uowMock.Verify(x => x.CommentRepository.GetForArticle(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public void ArticleConverterConvertToArticleTest()
        {
            ArticleInfo articleInfo = new ArticleInfo
            {
                Id = Guid.NewGuid(),
                Name = "Test article",
                Comments = new List<CommentInfo>()
            };

            Article article = Mapper.Map<ArticleInfo, Article>(articleInfo);

            Assert.Equal(article.Id, articleInfo.Id);
        }

        [Fact]
        public void ArticleConverterConvertToArticleInfoTest()
        {
            Article article = new Article
            {
                Id = Guid.NewGuid(),
                Name = "Test article",
            };

            var comments = new List<Comment>
            {
                new Comment
                {
                    Id = Guid.NewGuid(),
                    ArticleId = article.Id,
                    Content = "Comment content"
                }
            };

            var ai = Mapper.Map<ArticleInfo>(article);
            ai.Comments = Mapper.Map<List<CommentInfo>>(comments);
            
            Assert.Contains(ai.Comments, x => x.ArticleId == article.Id);
        }
    }
}
