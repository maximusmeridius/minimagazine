﻿using System;

namespace MiniMagazine.Contracts
{
    /// <summary>
    /// Описывает структуру комментария.
    /// </summary>
    public class CommentInfo
    {
        /// <summary>
        /// Уникальный идентификатор комментария.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Автор комментария.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Содержание комментария.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Идентификатор комментируемой статьи.
        /// </summary>
        public Guid ArticleId { get; set; }

        /// <summary>
        /// Дата публикации комментария.
        /// </summary>
        public DateTimeOffset PublicationDate { get; set; }
    }
}
