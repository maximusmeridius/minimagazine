﻿using System;
using System.Collections.Generic;

namespace MiniMagazine.Contracts
{
    /// <summary>
    /// Описывает структуру статьи.
    /// </summary>
    public class ArticleInfo
    {
        /// <summary>
        /// Уникальный идентификатор статьи.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Название статьи.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Содержание статьи.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Автор статьи.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Дата публикации статьи.
        /// </summary>
        public DateTimeOffset PublicationDate { get; set; }

        /// <summary>
        /// Ссылка на изображение к статье.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Список комментариев к статье.
        /// </summary>
        public List<CommentInfo> Comments { get; set; }
    }
}





