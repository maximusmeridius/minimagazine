﻿using System;

namespace MiniMagazine.Contracts
{
    /// <summary>
    /// Описывает структуру статьи-превью (Только id и название).
    /// </summary>
    public class ArticlePreview
    {
        /// <summary>
        /// Уникальный идентификатор статьи.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Название статьи.
        /// </summary>
        public string Name { get; set; }
    }
}