﻿// Взято с https://github.com/floydpink/CachedImage

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace MiniMagazine.WpfClient.Utils.CachedImage
{
    /// <summary>
    /// Implements the function of loading and caching images.
    /// </summary>
    public static class FileCache
    {
        // Record whether a file is being written.
        private static readonly Dictionary<string, bool> isWritingFile = new Dictionary<string, bool>();
        private static readonly string appCacheDirectory =
            $"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\\{Process.GetCurrentProcess().ProcessName}\\Cache\\";

        /// <summary>
        /// Gets or sets the path to the folder that stores the cache file. 
        /// </summary>
        public static async Task<MemoryStream> HitAsync(string url)
        {
            if (!Directory.Exists(appCacheDirectory))
            {
                Directory.CreateDirectory(appCacheDirectory);
            }
            var uri = new Uri(url);
            var fileNameBuilder = new StringBuilder();
            using (var sha1 = new SHA1Managed())
            {
                var canonicalUrl = uri.ToString();
                byte[] hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(canonicalUrl));
                fileNameBuilder.Append(BitConverter.ToString(hash).Replace("-", "").ToLower());
                if (Path.HasExtension(canonicalUrl))
                {
                    fileNameBuilder.Append(Path.GetExtension(canonicalUrl));
                }
            }

            var fileName = fileNameBuilder.ToString();
            var localFile = $"{appCacheDirectory}\\{fileName}";
            var memoryStream = new MemoryStream();

            FileStream fileStream = null;
            if (!isWritingFile.ContainsKey(fileName) && File.Exists(localFile))
            {
                using (fileStream = new FileStream(localFile, FileMode.Open, FileAccess.Read))
                {
                    await fileStream.CopyToAsync(memoryStream);
                }
                memoryStream.Seek(0, SeekOrigin.Begin);
                return memoryStream;
            }

            var request = WebRequest.Create(uri);
            request.Proxy.Credentials = CredentialCache.DefaultCredentials; 
            request.Timeout = 30;
            try
            {
                var response = await request.GetResponseAsync();
                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                    return null;
                if (!isWritingFile.ContainsKey(fileName))
                {
                    isWritingFile[fileName] = true;
                    fileStream = new FileStream(localFile, FileMode.Create, FileAccess.Write);
                }

                using (responseStream)
                {
                    var bytebuffer = new byte[100];
                    int bytesRead;
                    do
                    {
                        bytesRead = await responseStream.ReadAsync(bytebuffer, 0, 100);
                        if (fileStream != null)
                        {
                            await fileStream.WriteAsync(bytebuffer, 0, bytesRead);
                        }
                        await memoryStream.WriteAsync(bytebuffer, 0, bytesRead);
                    } while (bytesRead > 0);

                    if (fileStream != null)
                    {
                        await fileStream.FlushAsync();
                        fileStream.Dispose();
                        isWritingFile.Remove(fileName);
                    }
                }
                memoryStream.Seek(0, SeekOrigin.Begin);
                return memoryStream;
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }
    }
}