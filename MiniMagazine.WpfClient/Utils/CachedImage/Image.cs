﻿// Взято с https://github.com/floydpink/CachedImage

using System;
using System.Windows;
using System.Windows.Media.Imaging;
using Serilog;

namespace MiniMagazine.WpfClient.Utils.CachedImage
{
    /// <summary>
    /// Represents a control that is a wrapper on System.Windows.Controls.Image for enabling filesystem-based caching.
    /// </summary>
    public class Image : System.Windows.Controls.Image
    {
        /// <summary>
        /// Stores the current image's url
        /// </summary>
        public static readonly DependencyProperty ImageUrlProperty = DependencyProperty.Register("ImageUrl",
            typeof(string), typeof(Image), new PropertyMetadata("", ImageUrlPropertyChanged));


        /// <summary>
        /// Constructor.
        /// </summary>
        static Image()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Image),
                new FrameworkPropertyMetadata(typeof(Image)));
        }


        /// <summary>
        /// Remote image url.
        /// </summary>
        public string ImageUrl
        {
            get => (string)GetValue(ImageUrlProperty);
            set => SetValue(ImageUrlProperty, value);
        }


        private static async void ImageUrlPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var url = e.NewValue as string;

            if (string.IsNullOrEmpty(url))
                return;

            var cachedImage = (Image)obj;
            var bitmapImage = new BitmapImage();

            try
            {
                var memoryStream = await FileCache.HitAsync(url);
                if (memoryStream == null)
                {
                    return;
                }

                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.EndInit();
                cachedImage.Source = bitmapImage;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            
        }
    }
}