﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MiniMagazine.WpfClient.ServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference.IService")]
    public interface IService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetArticlesPreview", ReplyAction="http://tempuri.org/IService/GetArticlesPreviewResponse")]
        MiniMagazine.Contracts.ArticlePreview[] GetArticlesPreview();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetArticlesPreview", ReplyAction="http://tempuri.org/IService/GetArticlesPreviewResponse")]
        System.Threading.Tasks.Task<MiniMagazine.Contracts.ArticlePreview[]> GetArticlesPreviewAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/Get", ReplyAction="http://tempuri.org/IService/GetResponse")]
        MiniMagazine.Contracts.ArticleInfo Get(System.Guid id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/Get", ReplyAction="http://tempuri.org/IService/GetResponse")]
        System.Threading.Tasks.Task<MiniMagazine.Contracts.ArticleInfo> GetAsync(System.Guid id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/AddArticle", ReplyAction="http://tempuri.org/IService/AddArticleResponse")]
        System.Guid AddArticle(MiniMagazine.Contracts.ArticleInfo article);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/AddArticle", ReplyAction="http://tempuri.org/IService/AddArticleResponse")]
        System.Threading.Tasks.Task<System.Guid> AddArticleAsync(MiniMagazine.Contracts.ArticleInfo article);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/RemoveArticle", ReplyAction="http://tempuri.org/IService/RemoveArticleResponse")]
        void RemoveArticle(System.Guid id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/RemoveArticle", ReplyAction="http://tempuri.org/IService/RemoveArticleResponse")]
        System.Threading.Tasks.Task RemoveArticleAsync(System.Guid id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/Update", ReplyAction="http://tempuri.org/IService/UpdateResponse")]
        void Update(MiniMagazine.Contracts.ArticleInfo article);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/Update", ReplyAction="http://tempuri.org/IService/UpdateResponse")]
        System.Threading.Tasks.Task UpdateAsync(MiniMagazine.Contracts.ArticleInfo article);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/AddComment", ReplyAction="http://tempuri.org/IService/AddCommentResponse")]
        System.Guid AddComment(MiniMagazine.Contracts.CommentInfo comment);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/AddComment", ReplyAction="http://tempuri.org/IService/AddCommentResponse")]
        System.Threading.Tasks.Task<System.Guid> AddCommentAsync(MiniMagazine.Contracts.CommentInfo comment);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/RemoveComment", ReplyAction="http://tempuri.org/IService/RemoveCommentResponse")]
        void RemoveComment(System.Guid id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/RemoveComment", ReplyAction="http://tempuri.org/IService/RemoveCommentResponse")]
        System.Threading.Tasks.Task RemoveCommentAsync(System.Guid id);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceChannel : MiniMagazine.WpfClient.ServiceReference.IService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceClient : System.ServiceModel.ClientBase<MiniMagazine.WpfClient.ServiceReference.IService>, MiniMagazine.WpfClient.ServiceReference.IService {
        
        public ServiceClient() {
        }
        
        public ServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public MiniMagazine.Contracts.ArticlePreview[] GetArticlesPreview() {
            return base.Channel.GetArticlesPreview();
        }
        
        public System.Threading.Tasks.Task<MiniMagazine.Contracts.ArticlePreview[]> GetArticlesPreviewAsync() {
            return base.Channel.GetArticlesPreviewAsync();
        }
        
        public MiniMagazine.Contracts.ArticleInfo Get(System.Guid id) {
            return base.Channel.Get(id);
        }
        
        public System.Threading.Tasks.Task<MiniMagazine.Contracts.ArticleInfo> GetAsync(System.Guid id) {
            return base.Channel.GetAsync(id);
        }
        
        public System.Guid AddArticle(MiniMagazine.Contracts.ArticleInfo article) {
            return base.Channel.AddArticle(article);
        }
        
        public System.Threading.Tasks.Task<System.Guid> AddArticleAsync(MiniMagazine.Contracts.ArticleInfo article) {
            return base.Channel.AddArticleAsync(article);
        }
        
        public void RemoveArticle(System.Guid id) {
            base.Channel.RemoveArticle(id);
        }
        
        public System.Threading.Tasks.Task RemoveArticleAsync(System.Guid id) {
            return base.Channel.RemoveArticleAsync(id);
        }
        
        public void Update(MiniMagazine.Contracts.ArticleInfo article) {
            base.Channel.Update(article);
        }
        
        public System.Threading.Tasks.Task UpdateAsync(MiniMagazine.Contracts.ArticleInfo article) {
            return base.Channel.UpdateAsync(article);
        }
        
        public System.Guid AddComment(MiniMagazine.Contracts.CommentInfo comment) {
            return base.Channel.AddComment(comment);
        }
        
        public System.Threading.Tasks.Task<System.Guid> AddCommentAsync(MiniMagazine.Contracts.CommentInfo comment) {
            return base.Channel.AddCommentAsync(comment);
        }
        
        public void RemoveComment(System.Guid id) {
            base.Channel.RemoveComment(id);
        }
        
        public System.Threading.Tasks.Task RemoveCommentAsync(System.Guid id) {
            return base.Channel.RemoveCommentAsync(id);
        }
    }
}
