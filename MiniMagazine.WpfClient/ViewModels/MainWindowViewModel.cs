﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Apex.MVVM;
using AutoMapper;
using MiniMagazine.Contracts;
using MiniMagazine.WpfClient.ServiceReference;
using MiniMagazine.WpfClient.Views;

namespace MiniMagazine.WpfClient.ViewModels
{
    /// <summary>
    /// Описывает главную модель-представление.
    /// </summary>
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private readonly ServiceClient service = new ServiceClient();
        private ArticleInfo selectedArticle;
        private ArticlePreview selectedArticlePreview;
        private CommentInfo selectedComment;
        private CommentInfo addingComment = new CommentInfo();
        private ObservableCollection<ArticlePreview> articles;
        private ObservableCollection<CommentInfo> comments;

        /// <summary>
        /// Событие, происходящее при изменении свойства.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Конструктор.
        /// </summary>
        public MainWindowViewModel()
        {
            Articles = new ObservableCollection<ArticlePreview>(service.GetArticlesPreview());
            if (Articles.Count != 0)
            {
                selectedArticle = service.Get(articles[0].Id);
                Comments = new ObservableCollection<CommentInfo>(selectedArticle.Comments);
            }

            AddCommentCommand = new Command(AddComment);
            AddArticleCommand = new Command(AddArticle);
            EditArticleCommand = new Command(EditArticle);
            RemoveArticleCommand = new Command(RemoveArticle);
            RemoveCommentCommand = new Command<Guid>(RemoveComment);
        }


        /// <summary>
        /// Коллекция всех статей.
        /// </summary>
        public ObservableCollection<ArticlePreview> Articles
        {
            get => articles;
            set
            {
                articles = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Коллекция комментариев выбранной статьи.
        /// </summary>
        public ObservableCollection<CommentInfo> Comments
        {
            get => comments;
            set
            {
                comments = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Отображаемая статья.
        /// </summary>
        public ArticleInfo SelectedArticle
        {
            get => selectedArticle;
            set
            {
                selectedArticle = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Selected Article in the list.
        /// </summary>
        public ArticlePreview SelectedArticlePreview
        {
            get => selectedArticlePreview;
            set
            {
                selectedArticlePreview = value;
                OnPropertyChanged();
                if (selectedArticlePreview == null)
                {
                    return;
                }

                SelectedArticle = service.Get(selectedArticlePreview.Id);
                Comments = new ObservableCollection<CommentInfo>(selectedArticle.Comments);
            }
        }

        /// <summary>
        /// Выбранный из списка комментариев комментарий.
        /// </summary>
        public CommentInfo SelectedComment
        {
            get => selectedComment;
            set
            {
                selectedComment = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Добавляемый комментарий.
        /// </summary>
        public CommentInfo AddingComment
        {
            get => addingComment;
            set
            {
                addingComment = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Command "Add comment"
        /// </summary>
        public Command AddCommentCommand { get; set; }

        /// <summary>
        /// Command "Add article".
        /// </summary>
        public Command AddArticleCommand { get; set; }

        /// <summary>
        /// Command "Edit article".
        /// </summary>
        public Command EditArticleCommand { get; set; }

        /// <summary>
        /// Command "Remove article"
        /// </summary>
        public Command RemoveArticleCommand { get; set; }

        /// <summary>
        /// Command "Remove comment".
        /// </summary>
        public Command<Guid> RemoveCommentCommand { get; set; }


        private void AddComment()
        {
            addingComment.ArticleId = selectedArticle.Id;
            addingComment.PublicationDate = DateTimeOffset.Now;
            addingComment.Id = service.AddComment(addingComment);
            Comments.Add(addingComment);
            AddingComment = new CommentInfo();
        }


        private void RemoveComment(Guid id)
        {
            service.RemoveComment(id);
            Comments.Remove(comments.Single(x => x.Id == id));
        }


        private void AddArticle()
        {
            AddArticleWindowViewModel vm = new AddArticleWindowViewModel();
            AddArticleWindow window = new AddArticleWindow
            {
                DataContext = vm
            };
            window.ShowDialog();

            if (vm.DialogResult == AddArticleWindowViewModel.DialogResultEnum.Cancel)
            {
                return;
            }

            ArticleInfo article = vm.ResultArticle;
            article.Id = service.AddArticle(article);
            Articles.Add(Mapper.Map<ArticlePreview>(article));
        }


        private void EditArticle()
        {
            AddArticleWindowViewModel vm = new AddArticleWindowViewModel(SelectedArticle);
            AddArticleWindow window = new AddArticleWindow
            {
                DataContext = vm
            };
            window.ShowDialog();

            if (vm.DialogResult == AddArticleWindowViewModel.DialogResultEnum.Cancel)
            {
                return;
            }

            ArticleInfo article = vm.ResultArticle;
            service.Update(article);
            Articles[Articles.IndexOf(selectedArticlePreview)] = Mapper.Map<ArticlePreview>(article);
        }


        private void RemoveArticle()
        {
            service.RemoveArticle(selectedArticlePreview.Id);
            articles.Remove(articles.Single(x => x.Id == selectedArticlePreview.Id));
            SelectedArticlePreview = articles?[0];
        }


        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}


