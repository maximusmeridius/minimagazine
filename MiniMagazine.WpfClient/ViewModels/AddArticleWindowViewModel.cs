﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Apex.MVVM;
using MiniMagazine.Contracts;

namespace MiniMagazine.WpfClient.ViewModels
{
    /// <summary>
    /// Describes the "Add article" ViewModel.
    /// </summary>
    public class AddArticleWindowViewModel : INotifyPropertyChanged
    {
        private ArticleInfo resultArticle;
        private Command<Window> okCommand;
        private Command<Window> cancelCommand;

        /// <summary>
        /// Calls when a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Represents variants of closing "Add article" window.
        /// </summary>
        public enum DialogResultEnum
        {
            Ok,
            Cancel
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        public AddArticleWindowViewModel()
        {
            ResultArticle = new ArticleInfo();
            okCommand = new Command<Window>(Ok);
            cancelCommand = new Command<Window>(Cancel);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="article">Editing article.</param>
        public AddArticleWindowViewModel(ArticleInfo article)
        {
            ResultArticle = article;
            okCommand = new Command<Window>(Ok);
            cancelCommand = new Command<Window>(Cancel);
        }


        /// <summary>
        /// The variant of closing "Add article" window.
        /// </summary>
        public DialogResultEnum DialogResult { get; set; } = DialogResultEnum.Cancel;

        /// <summary>
        /// Adding/editing article.
        /// </summary>
        public ArticleInfo ResultArticle
        {
            get => resultArticle;
            set
            {
                resultArticle = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Command "Ok"
        /// </summary>
        public Command<Window> OkCommand
        {
            get => okCommand;
            set
            {
                okCommand = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Command "Cancel"
        /// </summary>
        public Command<Window> CancelCommand
        {
            get => cancelCommand;
            set
            {
                cancelCommand = value;
                OnPropertyChanged();
            }
        }


        private void Ok(Window window)
        {
            DialogResult = DialogResultEnum.Ok;
            ResultArticle.PublicationDate = DateTimeOffset.Now;
            window.Close();
        }

        private void Cancel(Window window)
        {
            DialogResult = DialogResultEnum.Cancel;
            window.Close();
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
