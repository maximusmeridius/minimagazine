﻿using System.Data;

namespace MiniMagazine.Application.Infrastructure
{
    /// <summary>
    /// Describes method to create connection.
    /// </summary>
    public interface IConnectionFactory
    {
        /// <summary>
        /// Returns a connection to server.
        /// </summary>
        /// <returns>Connection to database server.</returns>
        IDbConnection GetConnection();
    }
}
