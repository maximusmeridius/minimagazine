﻿using System.Data;
using Npgsql;

namespace MiniMagazine.Application.Infrastructure
{
    /// <summary>
    /// Determines methods to create PostgreSQL connection.
    /// </summary>
    public class PostgresConnectionFactory : IConnectionFactory
    {
        private readonly string connectionString;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="connectionString">Connection string.</param>
        public PostgresConnectionFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <returns>Open connection to Postgresql server</returns>
        public IDbConnection GetConnection()
        {
            {
                NpgsqlConnection connection = new NpgsqlConnection(connectionString);
                connection.Open();
                return connection;
            }
        }
    }
}
