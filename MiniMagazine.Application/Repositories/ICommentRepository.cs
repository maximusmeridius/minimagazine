﻿using System;
using System.Collections.Generic;
using MiniMagazine.Application.Model;

namespace MiniMagazine.Application.Repositories
{
    /// <summary>
    /// Describes methods to interact with comment database.
    /// </summary>
    public interface ICommentRepository
    {
        /// <summary>
        /// Removes all comments from the selected article.
        /// </summary>
        /// <param name="id">Selected article's id.</param>
        void DeleteForArticle(Guid id);


        /// <summary>
        /// Removes comment by its id.
        /// </summary>
        /// <param name="id">Removing comment's id.</param>
        void Delete(Guid id);


        /// <summary>
        /// Returns all comments to the selected article.
        /// </summary>
        /// <param name="articleId">Selected article's id.</param>
        /// <returns>List of all comments to the article.</returns>
        List<Comment> GetForArticle(Guid articleId);


        /// <summary>
        /// Adds the comment to the comment database.
        /// </summary>
        /// <param name="comment">Adding comment.</param>
        void Add(Comment comment);
    }
}
