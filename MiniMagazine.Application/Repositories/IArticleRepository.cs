﻿using System;
using System.Collections.Generic;
using MiniMagazine.Application.Model;
using MiniMagazine.Contracts;

namespace MiniMagazine.Application.Repositories
{
    /// <summary>
    /// Describes methods to interact with article database.
    /// </summary>
    public interface IArticleRepository
    {
        /// <summary>
        /// Removes article by its id.
        /// </summary>
        /// <param name="id">Removing article's id.</param>
        void Delete(Guid id);


        /// <summary>
        /// Returns article (without comments).
        /// </summary>
        /// <param name="id">Article id.</param>
        /// <returns>Article (without comments).</returns>
        Article Get(Guid id);


        /// <summary>
        /// Gets all article previews (id, name).
        /// </summary>
        /// <returns>List of ArticlePreview.</returns>
        List<ArticlePreview> GetAllArticlePreviews();


        /// <summary>
        /// Adds the article to the database.
        /// </summary>
        /// <param name="article">Adding article.</param>
        void Add(Article article);


        /// <summary>
        /// Updates information about article. 
        /// </summary>
        /// <param name="article">Updating article.</param>
        void Update(Article article);
    }
}
