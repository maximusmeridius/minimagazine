﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using MiniMagazine.Application.Model;

namespace MiniMagazine.Application.Repositories
{
    /// <summary>
    /// Determines methods to work with comments database.
    /// </summary>
    public class SqlCommentsRepository : ICommentRepository
    {
        private readonly IDbTransaction transaction;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="transaction">IConnectionFactory implementing object.</param>
        public SqlCommentsRepository(IDbTransaction transaction)
        {
            this.transaction = transaction;
        }

        /// <inheritdoc/>
        public void Add(Comment comment)
        {
            string query = "INSERT INTO comments(id, articleid, content, author, publicationdate) VALUES(@id, @articleid, @content, @author, @publicationdate);";
            transaction.Connection.Execute(query,
                new
                {
                    comment.Id,
                    comment.ArticleId,
                    comment.Content,
                    comment.Author,
                    comment.PublicationDate
                }, 
                transaction);
        }

        /// <inheritdoc/>
        public void Delete(Guid id)
        {
            string query = "DELETE FROM public.comments WHERE @id = id;";
            transaction.Connection.Execute(query, new { id });
        }

        /// <inheritdoc/>
        public void DeleteForArticle(Guid articleId)
        {
            string removeCommentsQuery = "DELETE FROM comments WHERE articleid = @articleid;";
            transaction.Connection.Execute(removeCommentsQuery, new {articleId}, transaction);
        }

        /// <inheritdoc/>
        public List<Comment> GetForArticle(Guid articleId)
        {
            string query = "SELECT id, articleid, content, author, publicationdate FROM comments WHERE articleid = @articleid;";
            return transaction.Connection.Query<Comment>(query, new { articleId }, transaction).ToList();
        }
    }
}
