﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using MiniMagazine.Application.Model;
using MiniMagazine.Contracts;

namespace MiniMagazine.Application.Repositories
{
    /// <summary>
    /// Determines methods to interact with articles database.
    /// <inheritdoc />
    /// </summary>
    public class SqlArticlesRepository : IArticleRepository
    {
        private readonly IDbTransaction transaction;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="transaction">IDbTransaction implementing object.</param>
        public SqlArticlesRepository(IDbTransaction transaction)
        {
            this.transaction = transaction;
        }


        /// <inheritdoc />
        public void Add(Article article)
        {
            string query = "INSERT INTO Articles(Id, Name, Content, ImageUrl, PublicationDate, Author) VALUES (@id, @name, @content, @imageurl, @publicationdate, @author);";
            transaction.Connection.Execute(query, article, transaction);
        }


        /// <inheritdoc />
        public void Delete(Guid id)
        {
            string removeArticleQuery = "DELETE FROM articles WHERE id = @id;";
            transaction.Connection.Execute(removeArticleQuery, new { id }, transaction);
        }


        /// <inheritdoc />
        public Article Get(Guid id)
        {
            string getArticleQuery = "SELECT Id, Name, Content, ImageUrl, PublicationDate, Author FROM Articles WHERE Id = @id;";
            return transaction.Connection.QueryFirst<Article>(getArticleQuery, new { id }, transaction);
        }


        /// <inheritdoc />
        public List<ArticlePreview> GetAllArticlePreviews()
        {
            string getArticlePreviewsQuery = "SELECT Id, Name FROM Articles";
            return transaction.Connection.Query<ArticlePreview>(getArticlePreviewsQuery, transaction: transaction).ToList();
        }


        /// <inheritdoc />
        public void Update(Article article)
        {
            string updateArticleQuery = "UPDATE Articles SET name=@name, content=@content, imageurl=@imageurl, publicationdate=@publicationdate, author=@author WHERE id=@id" ;
            transaction.Connection.Execute(updateArticleQuery, new
            {
                id = article.Id,
                name = article.Name,
                content = article.Content,
                imageurl = article.ImageUrl,
                publicationdate = article.PublicationDate,
                author = article.Author
            }, transaction);
        }
    }
}
