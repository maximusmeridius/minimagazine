﻿using System;
using System.Collections.Generic;
using MiniMagazine.Contracts;

namespace MiniMagazine.Application.Services
{
    /// <summary>
    /// Описывает методы для работы со статьями.
    /// </summary>
    public interface IArticleService
    {
        /// <summary>
        /// Возвращает список всех статей.
        /// </summary>
        /// <returns></returns>
        List<ArticlePreview> GetArticlesPreview();


        /// <summary>
        /// Returns selected article with comments.
        /// </summary>
        /// <param name="id">Article id.</param>
        /// <returns>Article and comments to it.</returns>
        ArticleInfo Get(Guid id);


        /// <summary>
        /// Добавляет статью в список статей.
        /// </summary>
        /// <param name="article">Добавляемая статья.</param>
        /// <returns>Идентификатор статьи.</returns>
        Guid AddArticle(ArticleInfo article);


        /// <summary>
        /// Удаляет статью из списка.
        /// </summary>
        /// <param name="id">Id удаляемой статьи.</param>
        void RemoveArticle(Guid id);


        /// <summary>
        /// Updates information about article.
        /// </summary>
        /// <param name="article">Updating article.</param>
        void UpdateArticle(ArticleInfo article);

        /// <summary>
        /// Добавляет комментарий
        /// </summary>
        /// <param name="comment"></param>
        /// <returns>Идентификатор комментария.</returns>
        Guid AddComment(CommentInfo comment);


        /// <summary>
        /// Удаляет комментарий.
        /// </summary>
        /// <param name="id">Id комментария.</param>
        void RemoveComment(Guid id);
    }
}
