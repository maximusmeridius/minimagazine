﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MiniMagazine.Application.Model;
using MiniMagazine.Application.UnitOfWork;
using MiniMagazine.Contracts;

namespace MiniMagazine.Application.Services
{
    /// <summary>
    /// Determines methods working with articles and comments.
    /// </summary>
    public class SqlArticleService : IArticleService
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="unitOfWorkFactory">Factory of UnitOfWork objects.</param>
        public SqlArticleService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }


        /// <inheritdoc />
        public List<ArticlePreview> GetArticlesPreview()
        {
            using (var uow = unitOfWorkFactory.Create())
            {
                return uow.ArticleRepository.GetAllArticlePreviews();
            }
        }


        /// <inheritdoc />
        public ArticleInfo Get(Guid id)
        {
            using (var uow = unitOfWorkFactory.Create())
            {
                Article article = uow.ArticleRepository.Get(id);
                List<Comment> comments = uow.CommentRepository.GetForArticle(id);
                var result = Mapper.Map<ArticleInfo>(article);
                result.Comments = Mapper.Map<List<CommentInfo>>(comments);
                return result;
            }
        }


        /// <inheritdoc />
        public Guid AddArticle(ArticleInfo article)
        {
            using (var uow = unitOfWorkFactory.Create())
            {
                article.Id = Guid.NewGuid();
                uow.ArticleRepository.Add(Mapper.Map<Article>(article));
                uow.Commit();
                return article.Id;
            }
        }


        /// <inheritdoc />
        public void RemoveArticle(Guid id)
        {
            using (var uow = unitOfWorkFactory.Create())
            {
                uow.CommentRepository.DeleteForArticle(id);
                uow.ArticleRepository.Delete(id);
                uow.Commit();
            }
        }

        /// <inheritdoc />
        public void UpdateArticle(ArticleInfo article)
        {
            using (var uow = unitOfWorkFactory.Create())
            {
                uow.ArticleRepository.Update(Mapper.Map<Article>(article));
                uow.Commit();
            }
        }

        /// <inheritdoc />
        public Guid AddComment(CommentInfo comment)
        {
            using (var uow = unitOfWorkFactory.Create())
            {
                comment.Id = Guid.NewGuid();
                uow.CommentRepository.Add(Mapper.Map<Comment>(comment));
                uow.Commit();
                return comment.Id;
            }
        }


        /// <inheritdoc />
        public void RemoveComment(Guid id)
        {
            using (var uow = unitOfWorkFactory.Create())
            {
                uow.CommentRepository.Delete(id);
                uow.Commit();
            }
        }
    }
}
