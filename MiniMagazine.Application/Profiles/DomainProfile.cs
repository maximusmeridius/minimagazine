﻿using AutoMapper;
using MiniMagazine.Application.Model;
using MiniMagazine.Contracts;

namespace MiniMagazine.Application.Profiles
{
    /// <summary>
    /// Mapper profile.
    /// </summary>
    public class DomainProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DomainProfile"/> class.
        /// </summary>
        public DomainProfile()
        {
            CreateMap<CommentInfo, Comment>();
            CreateMap<Comment, CommentInfo>();
            CreateMap<Article, ArticleInfo>();
            CreateMap<ArticleInfo, Article>();
        }
    }
}
