﻿using System;

namespace MiniMagazine.Application.Model
{
    /// <summary>
    /// Описывает структуру статьи.
    /// </summary>
    public class Article
    {
        /// <summary>
        /// Уникальный идентификатор статьи.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Название статьи.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Содержание статьи.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Автор статьи.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Дата публикации статьи.
        /// </summary>
        public DateTimeOffset PublicationDate { get; set; }

        /// <summary>
        /// Ссылка на изображение к статье.
        /// </summary>
        public string ImageUrl { get; set; }
    }
}





