﻿using System;
using MiniMagazine.Application.Repositories;

namespace MiniMagazine.Application.UnitOfWork
{
    /// <summary>
    /// Describes methods and properties to realise Unit of work pattern.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Repository to interact with articles in database.
        /// </summary>
        IArticleRepository ArticleRepository { get; }

        /// <summary>
        /// Repository to interact with comments in database.
        /// </summary>
        ICommentRepository CommentRepository { get; }

        /// <summary>
        /// Applies all changes in repositories.
        /// </summary>
        void Commit();
    }
}