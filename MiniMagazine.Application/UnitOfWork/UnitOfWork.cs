﻿using System;
using System.Data;
using MiniMagazine.Application.Infrastructure;
using MiniMagazine.Application.Repositories;

namespace MiniMagazine.Application.UnitOfWork
{
    /// <summary>
    /// Realizes Unit of work pattern.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private ICommentRepository commentRepository;
        private IArticleRepository articleRepository;
        private IDbConnection connection;
        private IDbTransaction transaction;
        private bool disposed;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="connectionFactory"></param>
        public UnitOfWork(IConnectionFactory connectionFactory)
        {
            connection = connectionFactory.GetConnection();
            transaction = connection.BeginTransaction();
        }

        /// <inheritdoc />
        public IArticleRepository ArticleRepository => articleRepository ?? (articleRepository = new SqlArticlesRepository(transaction));

        /// <inheritdoc />
        public ICommentRepository CommentRepository => commentRepository ?? (commentRepository = new SqlCommentsRepository(transaction));


        /// <summary>
        /// Applies changes.
        /// </summary>
        public void Commit()
        {
            try
            {
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                transaction = connection.BeginTransaction();
                articleRepository = null;
                commentRepository = null;
            }
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                        transaction = null;
                    }
                    if (connection != null)
                    {
                        connection.Dispose();
                        connection = null;
                    }
                }
                disposed = true;
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }
        ~UnitOfWork()
        {
            Dispose(false);
        }

    }
}