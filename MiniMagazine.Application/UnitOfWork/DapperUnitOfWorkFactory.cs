﻿using MiniMagazine.Application.Infrastructure;

namespace MiniMagazine.Application.UnitOfWork
{
    /// <summary>
    /// Realizes factory of UnitOfWork objects by Dapper.
    /// </summary>
    public class DapperUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IConnectionFactory connectionFactory;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="connectionFactory"></param>
        public DapperUnitOfWorkFactory(IConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }

        /// <inheritdoc />
        public IUnitOfWork Create()
        {
            return new UnitOfWork(connectionFactory);
        }
    }
}
