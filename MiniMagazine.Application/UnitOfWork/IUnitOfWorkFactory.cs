﻿namespace MiniMagazine.Application.UnitOfWork
{
    /// <summary>
    /// A factory to create IUnitOfWork objects.
    /// </summary>
    public interface IUnitOfWorkFactory
    {
        /// <summary>
        /// Creates UnitOfWork object.
        /// </summary>
        /// <returns>UnitOfWork object.</returns>
        IUnitOfWork Create();
    }
}
