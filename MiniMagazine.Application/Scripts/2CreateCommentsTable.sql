﻿-- Table: public.comments

-- DROP TABLE public.comments;

CREATE TABLE IF NOT EXISTS public.comments
(
    id uuid NOT NULL,
    articleid uuid NOT NULL,
    content text COLLATE pg_catalog."default",
    author text COLLATE pg_catalog."default",
    publicationdate date,
    CONSTRAINT comments_pkey PRIMARY KEY (id),
    CONSTRAINT "Comments_fkey" FOREIGN KEY (articleid)
        REFERENCES public.articles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.comments
    OWNER to postgres;