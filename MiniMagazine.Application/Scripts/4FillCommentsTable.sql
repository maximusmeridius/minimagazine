--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5
-- Dumped by pg_dump version 10.5

-- Started on 2018-10-22 20:30:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 16422)
-- Name: comments; Type: TABLE; Schema: public; Owner: postgres
--

--
-- TOC entry 2796 (class 0 OID 16422)
-- Dependencies: 197
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.comments (id, articleid, content, author, publicationdate) VALUES ('1d69da90-47f2-4c2b-9e77-9599cdbeec3d', '90838ef0-15e8-4dc6-a97c-18d82820b99b', 'nice car', 'Max', '2018-10-18');
INSERT INTO public.comments (id, articleid, content, author, publicationdate) VALUES ('97c9d79b-9990-4bc5-a38f-e22a72976812', '90838ef0-15e8-4dc6-a97c-18d82820b99b', 'Great car!', 'Max', '2018-10-18');
INSERT INTO public.comments (id, articleid, content, author, publicationdate) VALUES ('42b90448-1ba4-46e3-a5bd-c5cf01714ab5', '90838ef0-15e8-4dc6-a97c-18d82820b99b', 'Стильная', 'Root', '2018-10-18');
INSERT INTO public.comments (id, articleid, content, author, publicationdate) VALUES ('6ba09abe-e811-4af7-97e7-8cbc288c88fd', '90838ef0-15e8-4dc6-a97c-18d82820b99b', 'Не то слово!', 'Max', '2018-10-18');
INSERT INTO public.comments (id, articleid, content, author, publicationdate) VALUES ('8562ff21-a6e9-4e67-bda5-45a6a4eb2de8', '90838ef0-15e8-4dc6-a97c-18d82820b99b', 'Всем привет!', 'ВАСЯН', '2018-10-18');
INSERT INTO public.comments (id, articleid, content, author, publicationdate) VALUES ('ae943595-8f88-492a-89d9-8135b8848924', '4f1be626-561f-4418-a5b8-b535489ea1a1', 'Умер, пока пытался прочесть название', 'Максим', '2018-10-18');
INSERT INTO public.comments (id, articleid, content, author, publicationdate) VALUES ('00000000-0000-0000-0000-000000000000', '6743b49b-42df-488f-bfb7-a4b95c4c71fb', 'qwe', 'qwe', '2018-10-22');
INSERT INTO public.comments (id, articleid, content, author, publicationdate) VALUES ('cfe2cdb0-7772-4a1f-a8a8-e75eb71c20d7', '6743b49b-42df-488f-bfb7-a4b95c4c71fb', 'i like this book!', 'reader', '2018-10-22');


--
-- TOC entry 2673 (class 2606 OID 16429)
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- TOC entry 2674 (class 2606 OID 16430)
-- Name: comments Comments_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT "Comments_fkey" FOREIGN KEY (articleid) REFERENCES public.articles(id);


-- Completed on 2018-10-22 20:30:09

--
-- PostgreSQL database dump complete
--

