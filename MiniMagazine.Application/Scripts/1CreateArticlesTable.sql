﻿-- Table: public.articles

-- DROP TABLE public.articles;

CREATE TABLE IF NOT EXISTS public.articles
(
    id uuid NOT NULL,
    name name NOT NULL,
    content text COLLATE pg_catalog."default",
    imageurl text COLLATE pg_catalog."default",
    publicationdate date,
    author text COLLATE pg_catalog."default",
    CONSTRAINT "Articles_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.articles
    OWNER to postgres;