﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using MiniMagazine.Application.Model;
using MiniMagazine.Contracts;

namespace MiniMagazine.Service
{
    /// <summary>
    /// Интерфейс определяет контракт службы. 
    /// </summary>
    [ServiceContract]
    public interface IService
    {
        /// <summary>
        /// Возвращает список всех статей.
        /// </summary>
        /// <returns>Список статей.</returns>
        [OperationContract]
        List<ArticlePreview> GetArticlesPreview();


        /// <summary>
        /// Returns article with its comments
        /// </summary>
        /// <param name="id">Article id</param>
        /// <returns>Article information</returns>
        [OperationContract]
        ArticleInfo Get(Guid id);


        /// <summary>
        /// Добавляет статью в список статей.
        /// </summary>
        /// <param name="article">Добавляемая статья.</param>
        [OperationContract]
        Guid AddArticle(ArticleInfo article);


        /// <summary>
        /// Удаляет статью из списка.
        /// </summary>
        /// <param name="id">Id удаляемой статьи.</param>
        [OperationContract]
        void RemoveArticle(Guid id);


        /// <summary>
        /// Updates information about article.
        /// </summary>
        /// <param name="article">Updating article.</param>
        [OperationContract]
        void Update(ArticleInfo article);


        /// <summary>
        /// Добавляет комментарий к статье.
        /// </summary>
        /// <param name="comment">Добавляемый комментарий.</param>
        [OperationContract]
        Guid AddComment(CommentInfo comment);


        /// <summary>
        /// Removes comment from database.
        /// </summary>
        /// <param name="id">Removing comment's id.</param>
        [OperationContract]
        void RemoveComment(Guid id);

    }
}
