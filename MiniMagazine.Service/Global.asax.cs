﻿using System;
using Autofac.Integration.Wcf;
using AutoMapper;
using MiniMagazine.Application.Profiles;
using Serilog;

namespace MiniMagazine.Service
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            AutofacHostFactory.Container = AutofacContainerBuilder.BuildContainer();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new DomainProfile());
            });

            Log.Logger = new LoggerConfiguration().WriteTo.File(AppDomain.CurrentDomain.BaseDirectory + "\\log.txt").CreateLogger();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}