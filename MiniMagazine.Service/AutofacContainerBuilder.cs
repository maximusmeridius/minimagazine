﻿using System.Configuration;
using Autofac;
using AutoMapper;
using MiniMagazine.Application.Infrastructure;
using MiniMagazine.Application.Model;
using MiniMagazine.Application.Repositories;
using MiniMagazine.Application.Services;
using MiniMagazine.Application.UnitOfWork;
using MiniMagazine.Contracts;

namespace MiniMagazine.Service
{
    /// <summary>
    /// Defines method to build AutoFac container.
    /// </summary>
    public static class AutofacContainerBuilder
    {
        /// <summary>
        /// Configures and builds Autofac IOC container.
        /// </summary>
        /// <returns>DI container.</returns>
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            builder.Register<IConnectionFactory>(x =>
                {
                    var connectionString = ConfigurationManager.ConnectionStrings["work"].ConnectionString;
                    return new PostgresConnectionFactory(connectionString);
                }
            ).SingleInstance();
            builder.RegisterType<SqlArticlesRepository>().As<IArticleRepository>();
            builder.RegisterType<SqlCommentsRepository>().As<ICommentRepository>();
            builder.RegisterType<DapperUnitOfWorkFactory>().As<IUnitOfWorkFactory>();
            builder.RegisterType<SqlArticleService>().As<IArticleService>();
            builder.RegisterType<Service>().As<IService>().Named<object>("ArticleService");

            return builder.Build();
        }

    }
}