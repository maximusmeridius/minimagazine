﻿using System;
using System.Collections.Generic;
using MiniMagazine.Application;
using MiniMagazine.Application.Model;
using MiniMagazine.Application.Services;
using MiniMagazine.Contracts;


namespace MiniMagazine.Service
{
    /// <summary>
    /// Реализует сервис для работы со статьями.
    /// </summary>
    public class Service : IService
    {
        private readonly IArticleService service;


        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="service">Класс для работы со статьями.</param>
        public Service(IArticleService service)
        {
            this.service = service;
        }


        /// <summary>
        /// Возвращает список всех статей.
        /// </summary>
        /// <returns>Список всех статей.</returns>
        public List<ArticlePreview> GetArticlesPreview()
        {
            return service.GetArticlesPreview();
        }

        /// <inheritdoc />
        public ArticleInfo Get(Guid id)
        {
            return service.Get(id);
        }

        /// <summary>
        /// Добавляет статью в список.
        /// </summary>
        /// <param name="article">Добавляемая статья.</param>
        public Guid AddArticle(ArticleInfo article)
        {
            return service.AddArticle(article);
        }


        /// <summary>
        /// Удаляет статью из списка.
        /// </summary>
        /// <param name="id">Id удаляемой статьи.</param>
        public void RemoveArticle(Guid id)
        {
            service.RemoveArticle(id);
        }


        /// <inheritdoc />
        public void Update(ArticleInfo article)
        {
            service.UpdateArticle(article);
        }

        /// <inheritdoc />
        public Guid AddComment(CommentInfo comment)
        {
            return service.AddComment(comment);
        }



        /// <inheritdoc />
        public void RemoveComment(Guid id)
        {
            service.RemoveComment(id);
        }
    }
}
